<?php
    if(empty($_REQUEST)){
       $caso="mal";
    } else {
        $caso="bien";
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
    </head>
    <body>
        <?php
            if ($caso == "bien"){
                var_dump($_REQUEST);
            } else {
                ?>
                <div>
                    <form name="f" action="ejercicio16.php">
                        numero<input type="text" name="nombre" value=""/>
                        <input type="submit" value="enviar" name="boton"/>
                    </form>
                </div>
                <?php
            }
            /*
             * la salida del ejercicio es:
             * C:\xampp\htdocs\moises\ejercicios\hoja1\ejercicio16.php:16:
                array (size=2)
                'nombre' => string '' (length=0)
                'boton' => string 'enviar' (length=6)
             * esta salida se produce porque no se han introducido datos en el formulario
             * C:\xampp\htdocs\moises\ejercicios\hoja1\ejercicio16.php:16:
                array (size=2)
                'nombre' => string '21' (length=2)
                'boton' => string 'enviar' (length=6
             * en esta salida se introducen datos en el formulario
             */
        ?>
    </body>
</html>
