<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style type="text/css">
            *{
                margin: 0px;
                padding: 0px;
            }
            span{
                width: 10px;
                height: 10px;
                background-color: #001ee7;
                display: inline-block;
            }
            div{
                height: 50px;
                display: inline-block;
                border:5px solid #ccc;
            }
        </style>
    </head>
    <body>
        <?php
            $datos = array(20,40,790,234,890);
            $todo=max($datos);
            foreach ($datos as $key => $value){
            $datos[$key]=(int) (100*$datos[$key]/$todo);
            }
            var_dump($datos);
            foreach ($datos as $value){
                echo "<div>";
                    for($c=0;$c<$value;$c++){
                        echo "<span></span>";
                    }
                echo "</div>";
                echo "<br>";
            }
            /*
             * la salida del programa es la siguiente:
             array(5) { [0]=> int(2) [1]=> int(4) [2]=> int(88) [3]=> int(26) [4]=> int(100) }
             * en la salida también aparecen unos gráficos.
             */
        ?>
    </body>
</html>
